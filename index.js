(function main() {
    document.getElementById('showAndHidePlay')
    .addEventListener('click', function () {
        const block = document.getElementById('showAndHideBlock');
        animaster().showAndHide(5000).play(block);
    });

    const worryAnimationHandler = animaster()
        .addMove(200, {x: 80, y: 0})
        .addMove(200, {x: 0, y: 0})
        .addMove(200, {x: 80, y: 0})
        .addMove(200, {x: 0, y: 0})
        .buildHandler();
    document.getElementById('buildHandlerTestBlock').addEventListener('click', worryAnimationHandler);

    document.getElementById('moveAndHidePlay')
    .addEventListener('click', function () {
        const block = document.getElementById('moveAndHideBlock');
        const play = document.getElementById('moveAndHidePlay');
        const reset = document.getElementById('moveAndHideReset');
        animaster().moveAndHide(block, 5000, { x: 100, y: 10 }).play(block);

        disableButton(play);
        activateButton(reset);

        document.getElementById('moveAndHideReset')
            .addEventListener('click', function () {
                animaster().resetMoveAndHide(block);
                activateButton(play);
                disableButton(reset);
            });
    });

    document.getElementById('heartBeatingPlay')
        .addEventListener('click', function () {
            const block = document.getElementById('heartBeatingBlock');
            const playButton = document.getElementById('heartBeatingPlay');
            const stopButton = document.getElementById('heartBeatingStop');
            let heartBeatingPlay = animaster().play(block, true);
            
            
            disableButton(playButton);
            activateButton(stopButton);
            
            document.getElementById('heartBeatingStop')
                .addEventListener('click', function () {
                    heartBeatingPlay.stop();
                    activateButton(playButton);
                    disableButton(stopButton);
                });
        });
    
    document.getElementById('roundHeartBeatingPlay')
        .addEventListener('click', function () {
            const block = document.getElementById('roundHeartBeatingBlock');
            const playButton = document.getElementById('roundHeartBeatingPlay');
            const stopButton = document.getElementById('roundHeartBeatingStop');
            let heartBeatingPlay = animaster().roundHeartBeating(block);
            
            disableButton(playButton);
            activateButton(stopButton);
            
            document.getElementById('roundHeartBeatingStop')
                .addEventListener('click', function () {
                    heartBeatingPlay.stop();
                    activateButton(playButton);
                    disableButton(stopButton);
                });
        });

    document.getElementById('shakingPlay')
        .addEventListener('click', function () {
            const block = document.getElementById('shakingBlock');
            const playButton = document.getElementById('shakingPlay');
            const stopButton = document.getElementById('shakingStop');
            let shakingPlay = animaster().shaking(block);

            disableButton(playButton);
            activateButton(stopButton);

            document.getElementById('shakingStop')
                .addEventListener('click', function () {
                    shakingPlay.stop();
                    activateButton(playButton);
                    disableButton(stopButton);
                });
        });

    document.getElementById('fadeInPlay')
        .addEventListener('click', function () {
            const block = document.getElementById('fadeInBlock');
            const reset = document.getElementById('fadeInReset');            
            const play = document.getElementById('fadeInPlay');

            animaster().addFadeIn(5000).play(block);
            
            activateButton(reset);
            disableButton(play);

            document.getElementById('fadeInReset')
                .addEventListener('click', function () {
                    animaster().resetFadeIn(block);
                    activateButton(play);
                    disableButton(reset);
                });
        });
    

    document.getElementById('fadeOutPlay')
        .addEventListener('click', function () {
            const block = document.getElementById('fadeOutBlock');
            const reset = document.getElementById('fadeOutReset');            
            const play = document.getElementById('fadeOutPlay')

            animaster().addFadeOut(5000).play(block);

            activateButton(reset);
            disableButton(play);

            document.getElementById('fadeOutReset')
                .addEventListener('click', function () {
                    animaster().resetFadeOut(block);
                    activateButton(play);
                    disableButton(reset);
                });
        });

    

    document.getElementById('movePlay')
        .addEventListener('click', function () {
            const block = document.getElementById('moveBlock');            
            const reset = document.getElementById('moveReset');    
            const play = document.getElementById('movePlay');

            animaster().addMove(500, {x: 20, y:20}).play(block);

            activateButton(reset);
            disableButton(play);

            document.getElementById('moveReset')
                .addEventListener('click', function () {                                  
                    animaster().resetMoveAndScale(block);

                    activateButton(play);
                    disableButton(reset);
                });
        });
    
    document.getElementById('scalePlay')
        .addEventListener('click', function () {
            const block = document.getElementById('scaleBlock');
            const reset = document.getElementById('scaleReset');            
            const play = document.getElementById('scalePlay');

            animaster().addScale(1000, 1.25).play(block);

            activateButton(reset);
            disableButton(play);

            document.getElementById('scaleReset')
                .addEventListener('click', function () {                                  
                    animaster().resetMoveAndScale(block);
                    activateButton(play);
                    disableButton(reset);
                });
        });

    document.getElementById('customAnimationPlay')
        .addEventListener('click', function () {
            const block = document.getElementById('customAnimationBlock');
            const reset = document.getElementById('customAnimationReset');            
            const play = document.getElementById('customAnimationPlay');

            const customAnimation = animaster()
                    .addMove(200, {x: 40, y: 40})
                    .addScale(800, 1.3)
                    .addMove(200, {x: 80, y: 0})
                    .addScale(800, 1)
                    .addMove(200, {x: 40, y: -40}) 
                    .addScale(800, 0.7)
                    .addMove(200, {x: 0, y: 0})
                    .addScale(800, 1); 

            customAnimation.play(block);

            activateButton(reset);
            disableButton(play);

            document.getElementById('customAnimationReset')
                .addEventListener('click', function () {                                  
                    animaster().resetMoveAndScale(block);
                    activateButton(play);
                    disableButton(reset);
                });
        });
        
})();

function disableButton(button) {
    button.disabled = true;
    button.style.cursor = "default";
}

function activateButton(button) {
    button.disabled = false;
    button.style.cursor = "pointer";
}

/**
 * Блок плавно появляется из прозрачного.
 * @param element — HTMLElement, который надо анимировать
 * @param duration — Продолжительность анимации в миллисекундах
 * @param translation — объект с полями x и y, обозначающими смещение блока
 * @param ratio — во сколько раз увеличить/уменьшить. Чтобы уменьшить, нужно передать значение меньше 1
 */
function animaster() {
    let _steps = [];

    function addMove(duration, translation) {
        let step = {};
        step['title'] = 'move';
        step['duration'] = duration;
        step['translation'] = translation;
        _steps.push(step);
        return this;
    }

    function addScale(duration, ratio) {
        let step = {};
        step['title'] = 'scale';
        step['duration'] = duration;
        step['ratio'] = ratio;
        _steps.push(step);
        return this;
    }

    function addFadeIn(duration) {
        let step = {};
        step['title'] = 'fadein';
        step['duration'] = duration;
        _steps.push(step);
        return this;
    }

    function addFadeOut(duration) {
        let step = {};
        step['title'] = 'fadeout';
        step['duration'] = duration;
        _steps.push(step);
        return this;
    }   

    function addDelay(duration) {
        (duration) => {
            setTimeout(() => {}, duration);
          }
        let step = {};
        step['title'] = 'delay';
        step['duration'] = duration;
        _steps.push(step);
        return this;
    }
//old play
    // function play(element, cycled) {
    //     let timerDelay = 0;
    //     let cycledAnimation;
    //     for (let i = 0; i < _steps.length; i++) {
    //         let duration = _steps[i].duration;
    //         let translation = _steps[i].translation;
    //         let ratio = _steps[i].ratio;
    //         let title = _steps[i].title;
    //         if (i !== 0) {
    //             timerDelay += _steps[i-1].duration;
    //         }
    //         setTimeout(() => {
    //             switch (title) {
    //                 case 'move':
    //                     move(element, duration, translation);
    //                     break;
    //                 case 'scale':
    //                     scale(element, duration, ratio)
    //                     break;
    //                 case 'fadein':
    //                     fadeIn(element, duration);
    //                     break;
    //                 case 'fadeout':
    //                     fadeOut(element, duration);
    //                     break;
    //                 case 'delay':
    //                     addDelay(element, duration);
    //                     break;
                   
    //             }
    //         }, timerDelay)
    //     }
    //     if (cycled) {
    //         cycledAnimation = setInterval(() => heartBeating(element), 500);
    //       }
    // }
    function play(element, cycled) {
        let cycledAnimation;
        function playAnimStep(stepIndex, cycled) {
            const step = _steps[stepIndex];
                switch (step.title) {
                    case 'move':
                        move(element, duration, translation);
                        break;
                    case 'scale':
                        scale(element, duration, ratio)
                        break;
                    case 'fadein':
                        fadeIn(element, duration);
                        break;
                    case 'fadeout':
                        fadeOut(element, duration);
                        break;
                    case 'delay':
                        addDelay(element, duration);
                        break;                   
                }
                timeoutId = setTimeout(() => {
                    if(stepIndex + 1, cycled < _steps.length) {
                        playAnimStep(stepIndex + 1, cycled)
                    } else if (cycled) {
                        //цикличная анимация
                        cycledAnimation = setInterval(
                            () => playAnimStep(stepIndex), duration)
                    }
                }, duration);
        }
        playAnimStep(0, cycled)
        return {
            stop: () => {
                //timeoutId
                // if (cycled) {
                //     clearInterval(cycledAnimation);
                //     clearTimeout(timeoutId);
                //     playAnimStep(0);
                // }
            },
            reset: () => {
                //шаги отмены
                // clearTimeout(timeoutId);
                // playAnimStep(0);
            }
        }
    }

    function buildHandler() {
        let animasterThis = this;
        return function () {
            animasterThis.play(this);
        }
    }

    function fadeIn(element, duration) {
        element.style.transitionDuration = `${duration}ms`;
        element.classList.remove('hide');
        element.classList.add('show');
    }

    function fadeOut(element, duration) {
        element.style.transitionDuration = `${duration}ms`;
        element.classList.add('hide');
        element.classList.remove('show');
    }

    function move(element, duration, translation) {
        element.style.transitionDuration = `${duration}ms`;
        element.style.transform = getTransform(translation, null);
    }

    function scale(element, duration, ratio) {
        element.style.transitionDuration = `${duration}ms`;
        element.style.transform = getTransform(null, ratio);
    }

    function moveAndHide(element, duration, translation) {
        //old code      
        // element.style.transitionDuration = `${2*duration/5}ms`;
        // element.style.transform = getTransform(translation, null);
        // function step2 (){
        //     element.style.transitionDuration = `${3*duration/5}ms`;
        //     element.classList.add('hide');
        // }  
        // let moveAndHideTimeout = setTimeout(step2, 2*duration/5); 

        // function stop() {
        //     element.style.transitionDuration = '0ms';
        //     element.style.transform = getTransform({ x: 0, y: 0 }, null);
        //     element.classList = "block";
        //     clearTimeout(moveAndHideTimeout);
        // }
        
        addMove((duration * 2) / 5, translation);
        addDelay((duration * 2) / 5);
        addFadeOut((duration * 3) / 5);
        return this;

    }

    function showAndHide(duration) {
        //old code
        // element.style.transitionDuration = `${duration/3}ms`;
        // element.classList.remove('hide');
        // element.classList.add('show');
        // function step2 (){
        //     element.style.transitionDuration = `${duration/3}ms`;
        //     element.classList.remove('show');
        //     element.classList.add('hide');
        // }  
        // setTimeout(step2, 2*duration/3);
        addFadeIn(duration / 3);
        addDelay(duration / 3);
        addFadeOut(duration / 3);
        return this;

    }

    function heartBeating(element) {
        //old code
        // let isNormalSize = true;
        // element.style.transitionDuration = '500ms';
        // let heartBeatingLoop = setInterval(function() {
        //     if (isNormalSize === true) {
        //         element.style.transform = getTransform(null, 1.4);
        //         isNormalSize = false;
        //     } else {
        //         element.style.transform = getTransform(null, 1);
        //         isNormalSize = true;
        //     }
        // }, 500);
        // function stop() {
        //     element.style.transitionDuration = '0ms';
        //     element.style.transform = getTransform(null, 1);
        //     isNormalSize = true;
        //     clearInterval(heartBeatingLoop);            
        // }
        // return {stop};

        // let heartBeatingLoop = setInterval(function() {
            addScale(500, 1.4);
            addScale(500, 1);
            play(element);        
            // }, 500); 

            return {
                stop() {
                    element.style.transitionDuration = '0ms';
                    element.style.transform = getTransform(null, 1);
                    // clearInterval(heartBeatingLoop);
                }                
            }           
    }
      
    

    function shaking(element) { 
        let isStartLocation = true;
        element.style.transitionDuration = '500ms';
        let shakingLoop = setInterval(function() {
            if (isStartLocation === true) {
                element.style.transform = getTransform({ x: 20, y: 0 }, null);
                isStartLocation = false;
            } else {
                element.style.transform = getTransform({ x: 0, y: 0 }, null);
                isStartLocation = true;
            }
        }, 500);

        function stop() {
            element.style.transitionDuration = '0ms';
            element.style.transform = getTransform({ x: 0, y: 0 }, null);
            isStartLocation = true;
            clearInterval(shakingLoop);
        }

        return this;
    }

    function roundHeartBeating(element) {
        let isNormalSize = true;
        element.style.transitionDuration = '250ms';
        let roundLoop = setInterval(function() {
            if (isNormalSize === true) {
                element.style.borderRadius = '50%';
                isNormalSize = false;
            } else {
                element.style.borderRadius = '0%';
                isNormalSize = true;
            }
        }, 250);
        
        function stop() {
            element.style.transitionDuration = '0ms';
            element.style.borderRadius = '0%';
            isNormalSize = true;
            clearInterval(roundLoop);
        }

        return {stop};
    }

    function resetElem (element, classListParam) {
        element.classList = classListParam;
        element.style = null;
    }

    function resetFadeIn(element){       
        resetElem(element, "block hide");
    }

    function resetFadeOut (element) {
        resetElem(element, "block");
    }
    function resetMoveAndScale (element) {
        resetElem(element, "block");
    }
    function resetMoveAndHide (element) {
        resetElem(element, "block");
    }

    function getTransform(translation, ratio, borderRadius) {
        const result = [];
        if (translation) {
            result.push(`translate(${translation.x}px,${translation.y}px)`);
        }
        if (ratio) {
            result.push(`scale(${ratio})`);
        }
        return result.join(' ');
    }

    function getTransform(translation, ratio) {
        const result = [];
        if (translation) {
            result.push(`translate(${translation.x}px,${translation.y}px)`);
        }
        if (ratio) {
            result.push(`scale(${ratio})`);
        }
        return result.join(' ');
    }

    return {fadeIn, fadeOut, move, 
            scale, moveAndHide, showAndHide, 
            heartBeating, shaking, resetFadeIn, 
            resetFadeOut, resetMoveAndScale, resetMoveAndHide, play, 
            addMove, addScale, addFadeIn,
            addFadeOut, buildHandler, roundHeartBeating,
            addDelay, stop};
}